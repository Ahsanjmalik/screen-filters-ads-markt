Facial-recognition
Facial recognition and gender classification project for CSC411. 
Included is the script for parsing the URL and bounding boxes in the text file to 
download the images. The system is built more or less from scratch using a linear regression
model with gradient descent to minimize the cost function. See the attached PDF for a more 
in-depth explanation.